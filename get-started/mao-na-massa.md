# Preparando o ambiente.

> Agora vamos seguir todos os passos necessários para rodar o projeto.

A Aplicação usa containers docker para subir o ambiente de desenvolvimento, você não precisa instalar apache, php ou qualquer outro software, após ter o docker pronto, instalado na sua máquina é necessário apenas subir as containers que rodam o projeto 

### Instalando o Docker:

##### Ubuntu

Primeiro atualize seu sistema, com:

``bash
    sudo apt-get update -y
``

Depois, execute o seguinte comando para poder instalar os pré-requisitos:

``bash
    sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common
``
Agora, adicione a chave do docker com o seguinte comando:

``bash
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
``

Agora, para o Ubuntu baseado em arquitetura adm64, rode o seguinte comando:

``bash
    sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu zesty stable"
``

Agora que já temos o novo repositório no nosso SO, temos que atualizar o Ubuntu para ele saber quais são os pacotes desse novo repositório. Para isso, execute o seguinte comando:

``bash
    sudo apt-get update -y
``

E agora, vamos realmente instalar o docker com o seguinte comando:

``bash
    sudo apt-get install -y docker-ce
``

Para testar se está tudo ok, execute o container do docker chamado hello-world, com o seguinte comando:

``bash
    sudo docker run hello-world
``

Você deverá ter um output parecido com:
![Scrren Shot resultado do run hello-world](_media/screenshot_01.png)


##### macOS

Primeiro atualize seu sistema, com:

##### Windows

https://www.evernote.com/l/AhpNd10hC2xPNqzJYLtSIb8Zggs4wW7ZZcA

##### Instalando o Docker Compose:
Instalação do docker-compose:
https://www.evernote.com/l/AhpZoGfQd2pGJJA0wTIKC9dx_v-zyKRPMuE

Com docker instalado, você já pode montar o ambiente rodando poucos comandos. Antes de irmos para o terminal, precisamos instalar o Gcloud, é lá onde estão as imagens docker. 

##### Instalando o Gcloud
https://www.evernote.com/l/Ahq1fQhsIA1M2q-PwpPb0_pRUrMxhnK7ppY (edited) 