# Como as coisas funcionam ☕

Vamos começar a entender um pouco mais do que estamos falando:

### De praxe

Todos os sistemas tem em comum a seguinte stack:

- Docker / <small>[❓](https://www.docker.io "Está com dúvidas? Veja aqui")</small>
- Git / <small>Gitlab para maioria dos projetos</small>
- Javascript / <small>ES5</small>
- PHP 5.3
- Mysql xx
- Apache
- Klex / <small>Tecnologia desenvolvida para interceptar as requisições no apache, fazendo com que a resposta ganhe um "gás" [❓](https://www.google.com "Está com dúvidas? Veja aqui")</small>

**Fica a seu critério decidir quais ferramentas usar. Hoje, a maioria das pessoas envolvidas no projeto usam o sistema operacional _Linux Mint_ e o editor _Visual Studio Code_, isso pode ajudar em situações que podem gerar problemas parecidos.**

## Magazines

O front final (sistema web onde o usuário faz a compra) é composto pela **stack**:

- SASS / <small>Não usamos SCSS, padrão SASS</small>
- NPM / <small>Não usamos Node, apenas o package menager</small>  
- Jquery / <small>Vesão 3.3.1</small>  
- Gulp / <small>Transpile SASS, minifica JS e CSS, organiza depêndencias do NPM </small>
- Behat 

**além das tecnologias "de praxe"**

## Infrashop Admin

A área administrativa do e-commerce

- ExtJS / <small></small>

**além das tecnologias "de praxe"**