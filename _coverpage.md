<!-- _coverpage.md -->

![logo](_media/logo.png)

# Infrashop <small>3.0</small>

> Documentação técnica dos sistemas que compõem <strong>i9xpPlatform</strong>.

<!-- - Esse é um subtitulo 1; -->
<!-- - Esse é um outro lorem subtitulo  -->

[Conheça a plataforma](https://infrashop.com.br/3.0/)
[Primeiros passos!](/get-started)