# O que é i9xpPlatform? 🤔

i9xpPlatform é uma plataforma de e-commerce robusta e madura que está no mercado a mais de 10 anos e continua em constante evolução. 
Essa plataforma foi constrída ao longo dos anos, ganhando corpo, se adaptando a novas necessidades do mercando e ganhando novas funcionalidades a cada ano.

Essa documentação foi feita para centralizar toda informação técnica (global) da plataforma i9xpPlatform.
Ela pode (deve) ser usada como base para o início da documentação técnica no caso de um e-commerce (cliente) que tem regras de negócio específica e precisam ser documentadas. 

<hr />

Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.


> Um framework composto por sistemas que atendem as seguintes necessidades:

- Magazines / <small>Faz o papel de front final, onde o cliente vai comprar, navegar e zapiar (tomar muito cuidado ao fazer manutenção em prod)</small>
- Gestão web / <small>Aqui é a área administrativa do site, podemos gerênciar banners, produtos, compras e etc</small>
- Gestão da informação / <small>Nesse sistema podemos ver tudo relacionado aos dados gerados no comportamento dos usuários (magazines).</small>